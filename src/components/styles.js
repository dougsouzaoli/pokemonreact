import styled from 'styled-components';

export const Container = styled.div`
  box-shadow: 0px 0px 8px rgba(0, 0, 0, 0.25);
  padding: 32px 32px 32px 32px;
  margin: 16px 0px 16px 0px;
  border-radius: 10px;
  width: 80%;
`;

export const ItemContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const Title = styled.h4`
  margin-right: 4px;
`;

export const Text = styled.span``;

export const ColumnContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

export const SpriteImg = styled.img``;
