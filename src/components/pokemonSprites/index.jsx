/* eslint-disable react/forbid-prop-types */
/* eslint-disable no-nested-ternary */
import React from 'react';
import PropTypes from 'prop-types';
import {
  Title, ItemContainer, ColumnContainer, SpriteImg,
} from '../styles';

const PokemonSprites = ({ sprites }) => (
  <ColumnContainer>
    <Title>Sprites:</Title>
    <ItemContainer>
      <SpriteImg src={sprites.back_default} />
      <SpriteImg src={sprites.back_shiny} />
      <SpriteImg src={sprites.front_default} />
      <SpriteImg src={sprites.front_shiny} />
    </ItemContainer>
  </ColumnContainer>
);

export default PokemonSprites;

PokemonSprites.propTypes = {
  sprites: PropTypes.object.isRequired,
};
