/* eslint-disable react/forbid-prop-types */
/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Text, Title, Container, ItemContainer, ColumnContainer,
} from '../styles';
import PokemonService from '../../services/PokemonService';
import PokemonAbilities from '../pokemonAbilities';
import PokemonSprites from '../pokemonSprites';

const PokemonItem = ({ item }) => {
  const [pokemon, setPokemon] = useState({
    abilities: [],
    forms: [],
    height: 0,
    moves: [],
    sprites: [],
    stats: [],
    types: [],
    weight: 0,
  });

  useEffect(() => {
    PokemonService.getPokemonByName(item.name)
      .then((response) => {
        setPokemon(response);
      });
  }, [item]);

  const joinFormatted = (array) => array.join(', ').replace(/, ([^,]*)$/, ' and $1.');

  return (
    <Container>
      <ItemContainer>
        <Title>Name:</Title>
        <Text>{item.name}</Text>
      </ItemContainer>
      <ItemContainer>
        <Title>Height:</Title>
        <Text>
          {pokemon.height}
          cm
        </Text>
      </ItemContainer>
      <ItemContainer>
        <Title>Weight:</Title>
        <Text>
          {pokemon.weight}
          kg
        </Text>
      </ItemContainer>
      <ItemContainer>
        <PokemonAbilities abilities={pokemon.abilities} />
      </ItemContainer>
      <ColumnContainer>
        <Title>Moves:</Title>
        <Text>
          {joinFormatted(pokemon.moves.map((elem) => elem.move.name))}
        </Text>
      </ColumnContainer>
      <ColumnContainer>
        <Title>Stats:</Title>
        <Text>
          {joinFormatted(pokemon.stats.map((elem) => elem.stat.name))}
        </Text>
      </ColumnContainer>
      <ColumnContainer>
        <Title>Types:</Title>
        <Text>
          {joinFormatted(pokemon.types.map((elem) => elem.type.name))}
        </Text>
      </ColumnContainer>
      <ItemContainer>
        <PokemonSprites sprites={pokemon.sprites} />
      </ItemContainer>
    </Container>
  );
};

export default PokemonItem;

PokemonItem.propTypes = {
  item: PropTypes.object.isRequired,
};
