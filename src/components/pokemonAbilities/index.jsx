/* eslint-disable react/no-array-index-key */
/* eslint-disable array-callback-return */
/* eslint-disable react/forbid-prop-types */
/* eslint-disable no-nested-ternary */
import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import {
  Text, Title, ColumnContainer,
} from '../styles';
import PokemonService from '../../services/PokemonService';

const PokemonAbilities = ({ abilities }) => {
  const [abilityDetails, setAbilityDetails] = useState([]);

  useEffect(() => {
    abilities.map((item) => {
      const id = item.ability.url.split('/')[6];
      PokemonService.getPokemonAbility(id)
        .then((response) => {
          setAbilityDetails((
            details,
          ) => [...details, { name: item.ability.name, details: response }]);
        });
    });
  }, [abilities]);

  return (
    <ColumnContainer key="list">
      <Title>Abilities:</Title>
      {abilityDetails.map((elem, index) => (
        <Text key={index}>
          {' '}
          -
          {elem.name}
          :
          {elem.details.effect_entries[0].short_effect}
        </Text>
      ))}
    </ColumnContainer>
  );
};

export default PokemonAbilities;

PokemonAbilities.propTypes = {
  abilities: PropTypes.array.isRequired,
};
