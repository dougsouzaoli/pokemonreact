import styled from 'styled-components';

export const Container = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`;

export const Image = styled.img`
  display: flex;
  margin-top: 32px;
  margin-bottom: 32px;
  justify-content: center;
`;

export const InfiniteContainer = styled.div`
  height: 800px;
  margin-top: 50px;
  margin-left: 82px;
  margin-right: 82px;
`;
