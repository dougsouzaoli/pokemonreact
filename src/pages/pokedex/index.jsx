/* eslint-disable react/no-array-index-key */
/* eslint-disable no-undef */
/* eslint-disable no-underscore-dangle */
import React, { useState, useEffect } from 'react';
import debounce from 'lodash.debounce';
import { Container, Image } from './styles';
import pokemonLogo from '../../assets/images/pokemon.png';
import PokemonService from '../../services/PokemonService';
import PokemonItem from '../../components/pokemonItem';

const Pokedex = () => {
  const [offset, setOffset] = useState(0);
  const [pokemons, setPokemons] = useState([]);
  const [hasMorePokemons, setHasMorePokemons] = useState(true);

  const getPokemons = () => {
    PokemonService.getPokemons(offset)
      .then((response) => {
        setPokemons([...pokemons, ...response.results]);
        setOffset(offset + 20);
        if (response.count < offset) setHasMorePokemons(false);
      });
  };

  useEffect(() => {
    getPokemons();
  }, []);

  useEffect(() => {
    window.onscroll = debounce(() => {
      if (!hasMorePokemons) return;
      if (
        window.innerHeight + document.documentElement.scrollTop
        === document.documentElement.offsetHeight
      ) {
        getPokemons();
      }
    }, 100);
  });

  return (
    <>
      <Container>
        <Image src={pokemonLogo} width="20%" />
        {pokemons.map((item, index) => <PokemonItem item={item} key={index} />)}
      </Container>
    </>
  );
};

export default Pokedex;
