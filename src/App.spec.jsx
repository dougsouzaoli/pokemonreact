import React from 'react';
import renderer from 'react-test-renderer';
import Pokedex from './pages/pokedex';
import PokemonAbilities from './components/pokemonAbilities';
import PokemonItem from './components/pokemonItem';
import PokemonSprites from './components/pokemonSprites';

/* eslint-disable no-undef */
test('Pokedex Test', () => {
  const component = renderer.create(
    <Pokedex />,
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('PokemonAbilities Test', () => {
  const component = renderer.create(
    <PokemonAbilities abilities={[]} />,
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('PokemonItem Test', () => {
  const component = renderer.create(
    <PokemonItem item={{}} />,
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('PokemonSprites Test', () => {
  const component = renderer.create(
    <PokemonSprites sprites={{}} />,
  );
  const tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});
