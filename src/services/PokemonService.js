import axios from '../middlewares/axios';

class PokemonService {
  static getPokemons(offset) {
    const url = `/pokemon/?offset=${offset}&limit=20`;
    return axios.get(url)
      .then((response) => response.data)
      .catch((error) => { throw error; });
  }

  static getPokemonByName(name) {
    const url = `/pokemon/${name}`;
    return axios.get(url)
      .then((response) => ({
        abilities: response.data.abilities,
        forms: response.data.forms,
        height: response.data.height,
        moves: response.data.moves,
        sprites: response.data.sprites,
        stats: response.data.stats,
        types: response.data.types,
        weight: response.data.weight,
      }))
      .catch((error) => { throw error; });
  }

  static getPokemonAbility(id) {
    const url = `/ability/${id}/`;
    return axios.get(url)
      .then((response) => response.data)
      .catch((error) => { throw error; });
  }

  static getPokemonMove(id) {
    const url = `/move/${id}/`;
    return axios.get(url)
      .then((response) => response.data)
      .catch((error) => { throw error; });
  }

  static getPokemonType(id) {
    const url = `/type/${id}/`;
    return axios.get(url)
      .then((response) => response.data)
      .catch((error) => { throw error; });
  }

  static getPokemonStat(id) {
    const url = `/stat/${id}/`;
    return axios.get(url)
      .then((response) => response.data)
      .catch((error) => { throw error; });
  }
}

export default PokemonService;
