## Tecnologias utilizadas:

1. GitLab;
2. ReactJS;

## Boas práticas utilizadas:

1. TDD com Jest e Snapshots: o snapshot testing renderiza o componente e identifica se o mesmo não teve nenhuma alteração inesperada, é um teste unitário importante de se fazer para garantir que a tela irá renderizar corretamente;
2. PropTypes: prática que realiza uma checagem dos tipos dos adereços passados aos componentes, garante que os mesmos irão ser passados corretamente para que não ocorram erros inesperados;
3. StyledComponents: utilização desta prática que ajuda na construção, estilização e reutilização de componentes;
4. ESLint: prática básica de análise de problemas no código, além de ajudar na correção automática de problemas de sintaxe.

* OBS: Outra prática importante é o uso de um arquivo .ENV para armezenar as variáveis principais do sistema, não utilizada por ser apenas uma URL de api e para facilitar na inicialização da aplicação.

## Comando para rodar o projeto:

* Pela primeira vez: "npm run init";
* Demais vezes: "npm start".

## Comando para rodar a checagem do ESLint:

* Rodar "npm run lint";

## Comando para rodar os testes unitários:

* Rodar "npm test";
